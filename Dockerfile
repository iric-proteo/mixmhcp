################## BASE IMAGE ######################

FROM biocontainers/biocontainers:latest

################## MAINTAINER ######################
MAINTAINER Mathieu Courcelles

USER root

RUN apt-get update && \
    apt-get install -y  \
        liblist-moreutils-perl && \
        apt-get clean && \
        apt-get purge && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN conda install -c r r  \
                       r-ggplot2 && \
    conda clean --all

WORKDIR /home/biodocker/bin


RUN     wget  https://github.com/GfellerLab/MixMHCp/archive/v2.1.tar.gz && \
        tar -xf v2.1.tar.gz && \
        rm v2.1.tar.gz && \
        cd /home/biodocker/bin/MixMHCp-2.1 && \
        sed -i 's/YOUR PATH TO MixMHCp2.1\/lib FOLDER/\/home\/biodocker\/bin\/MixMHCp-2.1\/lib/' MixMHCp && \
        g++ lib/MixMHCp.cc -o lib/MixMHCp.x && \
        mv lib/R/align.r lib/R/align.R && \
        mv lib/R/col_schemes.r lib/R/col_schemes.R && \
        mv lib/R/ggseqlogo.r lib/R/ggseqlogo.R && \
        mv lib/R/heights.r lib/R/heights.R 

WORKDIR /home/biodocker/bin/MixMHCp-2.1

# Test install
RUN ./MixMHCp -i test/test.txt -o test/out -m 3 && rm -rf test/out

ENV VERSION="MixMHCp v2.1"
ENV PATH=$PATH:/home/biodocker/bin/MixMHCp-2.1


ENTRYPOINT ["./MixMHCp"]
