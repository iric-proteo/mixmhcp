# MixMHCp container for MAPDP

This repository contains files to build a Docker container to run MixMHCp in MAPDP.


**Important notes**
* No Docker image is provided because of MixMHCp license restriction.
* Please read MixMHCp license before building this container.

Documentation and project license can be consulted here: 

* [MixMHCp](https://github.com/GfellerLab/MixMHCp)
* [MAPDP](https://gitlab.com/iric-proteo/mapdp)


## Build instructions

    $ git clone https://gitlab.com/iric-proteo/mixmhcp
    $ cd mixmhcp
    $ sudo docker build -t mixmhcp:build .
    
To use your Docker image in MAPDP, you must push it to your private registry:

    $ sudo docker build -t your_registry/mixmhcp:build .
    $ sudo docker push your_registry/mixmhcp:build

## MixMHCp usage

Use the following command to get instructions how to use MixMHCp.

    $ sudo docker run -it --rm -v /home/user/mixmhcp:/out mixmhcp:build --help

Copyright 2015-2020 Mathieu Courcelles

CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses

Pierre Thibault's lab

IRIC - Universite de Montreal